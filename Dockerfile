CREATE DATABASE sklep_odzieżowy;
CREATE TABLE dostawca (id_producenta INTEGER PRIMARY KEY,
 nazwa_producenta TEXT UNIQUE,
 adres_producenta TEXT UNIQUE,
 NIP_producenta CHAR(11) UNIQUE,
 data_podpisania_umowy_z_producentem DATE NOT NULL);

CREATE TABLE produkt (id_produktu INTEGER PRIMARY KEY,
nazwa_producenta TEXT,
nazwa_produktu TEXT NOT NULL,
opis_produktu TEXT NOT NULL,
cena_netto_zakupu DECIMAL (10,2) CHECK (cena_netto_zakupu >0),
cena_brutto_zakupu DECIMAL (10,2) CHECK (cena_brutto_zakupu >= cena_netto_zakupu),
cena_netto_sprzedaży DECIMAL (10,2) CHECK (cena_netto_sprzedaży >0),
cena_brutto_sprzedaży DECIMAL (10,2) CHECK (cena_brutto_sprzedaży >= cena_netto_sprzedaży),
procent_VAT_sprzedaży DECIMAL (3,2) CHECK (procent_VAT_sprzedaży >=0));

CREATE TABLE mapping_dostawca_id (nazwa_producenta TEXT UNIQUE,
id_producenta INTEGER, 
FOREIGN KEY (id_producenta) REFERENCES dostawca (id_producenta),
id_produktu INTEGER,
FOREIGN KEY (id_produktu) REFERENCES produkt (id_produktu));

CREATE TABLE zamówienie (id_zamówienia INTEGER PRIMARY KEY,
id_klienta INTEGER,
id_produktu INTEGER, FOREIGN KEY (id_produktu) REFERENCES produkt (id_produktu),
data_zamówienia DATE);

CREATE TABLE klient (id_klienta INTEGER PRIMARY KEY,
id_zamówienia INTEGER,
FOREIGN KEY (id_zamówienia) REFERENCES zamówienie (id_zamówienia),
imię TEXT,
nazwisko TEXT,
adres TEXT);

INSERT INTO  DOSTAWCA (id_producenta, nazwa_producenta, adres_producenta, NIP_producenta, data_podpisania_umowy_z_producentem) VALUES
(1, 'Ochnik S.A.', 'ul. Stacyjna 8B, 08-400 Garwolin', 8260000780, '2019-07-01'),
(2, 'Andrzej Jedynak', 'Aleje Jerozolimskie 23, 00-508 Warszawa', 1130007892, '2018-06-01'),
(3,'Fasardi', 'ul. Warszawska 44/50, 95-200 Pabianice', 7282804746, '2020-04-01'),
(4, 'Elementy', 'ul. Dobra, Warszawa', 1132918310, '2020-05-01');

INSERT INTO produkt (id_produktu, nazwa_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedaży, cena_brutto_sprzedaży, procent_VAT_sprzedaży) 
VALUES (1, 'Ochnik S.A.', 'koszula męska','koszula w kratę z kołnierzem', 39, NULL, 67.16, NULL, NULL),
(2, 'Ochnik S.A.', 'sweter męski','sweter męski czarny', 48, NULL, 82.66, NULL, NULL),
(3, 'Ochnik S.A.', 'spodnie męskie','spodnie proste z domieszką bawełny', 62, NULL, 106.76, NULL, NULL),
(4, 'Ochnik S.A.', 'płaszcz damski','płaszcz damski długi, wiązany w pasie', 300, NULL, 516.60, NULL, NULL),
(5, 'Ochnik S.A.', 'kurtka męska','kurtka rozpinana z kieszeniami', 250, NULL, 430.50, NULL, NULL),
(6, 'Andrzej Jedynak', 'koszula męska jedwabna','koszula jedwabna kolor khaki', 250, NULL, 430.50, NULL, NULL),
(7, 'Andrzej Jedynak', 'sweter męski wełniany','sweter wykonany w 100% z wełny', 180, NULL, 309.96, NULL, NULL),
(8, 'Andrzej Jedynak', 'spodnie męskie czarne','spodnie proste czarne', 120, NULL, 206.64, NULL, NULL),
(9, 'Andrzej Jedynak', 'płaszcz męski czarny','płaszcz z kieszeniami, zapinany na guziki', 450, NULL, 774.90, NULL, NULL),
(10, 'Andrzej Jedynak', 'kurtka męska czarna','kurtka męska ze ściągaczem w pasie, 300, NULL, 516.60, NULL, NULL),
(11, 'Fasardi', 'bluzka damska V','bluzka z dekoltem V, 62, NULL, 106.76, NULL, NULL),
(12, 'Fasardi', 'sweter damski różowy','sweter wykonany z wełny i poliestru', 70, NULL, 120.54, NULL, NULL),
(13, 'Fasardi', 'spodnie damskie','spodnie dopasowane z materiału', 120, NULL, 206.64, NULL, NULL),
(14, 'Fasardi', 'jeansy','niebieskie jeansy do kostek', 120, NULL, 206.64, NULL, NULL),
(15, 'Fasardi', 'body damskie','body damskie koronkowe czarne', 250, NULL, 430.50, NULL, NULL),
(16, 'Elementy', 'bluza damska dresowa','bluza z kapturem bawełniana z domieszką poliestru', 120, NULL, 206.64, NULL, NULL),
(17, 'Elementy', 'spodnie damskie dresowe','spodnie dresowe szare ze ściągaczem', 120, NULL, 206.64, NULL, NULL),
(18, 'Elementy', 'koszulka V','kuszulka z dekoltem V niebieska', 62, NULL, 106.76, NULL, NULL),
(19, 'Elementy', 'T-SHIRT','T-SHIRT z nadrukiem',48, NULL, 82.66, NULL, NULL),
(20, 'Elementy', 'sukienka w kwiaty','sukienka w kwiaty na czarnym tle', 180, NULL, 309.96, NULL, NULL);

UPDATE produkt SET procent_VAT_sprzedaży = 0,23 WHERE procent_VAT_sprzedaży IS NULL,
UPDATE produkt SET cena_brutto_zakupu = 1.23*cena_netto_zakupu WHERE cena_brutto_zakupu IS NULL,
UPDATE produkt SET cena_brutto_sprzedaży = 1.23*cena_netto_sprzedaży WHERE cena_brutto_sprzedaży IS NULL;

INSERT INTO zamówienie (id_zamówienia, id_klienta, id_produktu, data_zamówienia) VALUES
(1,3,12,'2021-01-12'),
(2,1,18,'2021-01-11'),
(3,2,7,'2021-01-15'),
(4,5,4,'2021-01-05'),
(5,4,20,'2021-01-03'),
(6,10,14,'2021-01-08'),
(7,8,16,'2021-01-08'),
(8,6,12,'2021-01-07'),
(9,7,3,'2021-01-05'),
(10,9,5,'2021-01-02')

INSERT INTO klient (id_klienta, id_zamówienia, imię, nazwisko, adres) VALUES
(1,2, 'Marcel', 'Koniuszy', 'Stalowa-Wola'),
(2,3, 'Elżbieta', 'Druga', 'Londyn'),
(3,1, 'Piotr', 'Gierazniński', 'Kaputy'),
(4,5, 'Adrianna', 'Panna', 'Wąsewo'),
(5,4, 'Aleksandra', 'Głodna', 'Kolorowo'),
(6,8, 'Michał', 'Blow', 'Dakota Południowa'),
(7,9, 'Robert', 'Klusek', 'Mokotów'),
(8,7, 'Jan', 'Potrawka', 'Wilanów'),
(9,10, 'Adam', 'Wajrak', 'Sulejówek'),
(10,6, 'Norebrt', 'Wąs', 'Targówek');

INSERT INTO mapping_dostawca_id (nazwa_producenta, id_producenta) VALUES
('Ochnik S.A.', 1), ('Andrzej Jedynak', 2), ('Fasardi', 3), ('Elementy', 4) 

SELECT* FROM produkt 
JOIN dostawca ON dostawca.id_produktu = produkt.id_produktu;

SELECT* FROM zamówienie 
JOIN produkt ON produkt.id_produktu = zamówienie.id_produktu;

SELECT* FROM zamówienie 
JOIN klient ON klient.id_zamówienia = zamówienie.id_zamówienia
ORDER BY zamówienie.id_zamówienia;

SELECT* FROM produkt 
JOIN dostawca ON dostawca.nazwa_producenta = produkt.nazwa_producenta 
WHERE id_producenta =1 ORDER BY nazwa_produktu;

SELECT AVG (cena_brutto_sprzedaży) FROM produkt
JOIN dostawca ON dostawca.nazwa_producenta = produkt.nazwa_producenta 
WHERE id_producenta =1;

SELECT nazwa_producenta, id_produktu, nazwa_produktu, CASE WHEN cena_brutto_sprzedaży <=200 THEN 'tanie' ELSE 'drogie' END
FROM produkt
WHERE nazwa_producenta = 'Ochnik S.A.';

SELECT nazwa_produktu FROM zamówienie
JOIN produkt ON zamówienie.id_produktu = produkt.id_produktu;

SELECT * FROM zamówienie LIMIT 5;

SELECT*, SUM(cena_brutto_sprzedaży) AS wartość_wszystkich_zamówień
FROM zamówienie
JOIN produkt ON produkt.id_produktu = zamówienie.id_produktu;

SELECT*
FROM zamówienie
JOIN produkt ON produkt.id_produktu = zamówienie.id_produktu
ORDER BY data_zamówienia;

SELECT * FROM produkt WHERE id_produktu IS NULL
OR nazwa_producenta IS NULL
OR nazwa_produktu IS NULL
OR opis_produktu IS NULL
OR cena_netto_zakupu IS NULL
OR cena_brutto_zakupu IS NULL
OR cena_netto_sprzedaży IS NULL
OR cena_brutto_sprzedaży IS NULL
OR procent_VAT_sprzedaży IS NULL;

SELECT zamówienie.id_produktu, nazwa_produktu, cena_brutto_sprzedaży 
FROM zamówienie
JOIN produkt ON produkt.id_produktu = zamówienie.id_produktu
GROUP BY zamówienie.id_produktu;

SELECT id_zamówienia, data_zamówienia
FROM zamówienie
GROUP BY data_zamówienia;
